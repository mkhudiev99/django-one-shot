from django.urls import path
from django.contrib.auth import views as auth_views

from todos.views import (
    # TodoListView,
    TodoListCreateView,
    TodoListDetailView,
    TodoListUpdateView,
    TodoListDeleteView,
    todo_list_list
)
from todos.views import (TodoItemCreateView, TodoItemUpdateView)

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_list_detail"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todo_list_delete"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_create"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todo_list_update"),
    path("items/create/", TodoItemCreateView.as_view(), name="todo_item_create"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todo_item_update"),
]