from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
# Create your views here.
from django.views.generic.list import ListView
from .models import TodoItem, TodoList
from django.views.generic.edit import CreateView, DeleteView, UpdateView


#  class TodoListView(ListView):
#     model = TodoList
#     template_name = "todos/list.html"

def todo_list_list(request):
    todosall = TodoList.objects.all()
    context = {"todolist_list": todosall}
    return render(request, "todos/list.html", context)


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    
    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context



class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    fields = ["task"]
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")



class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])

        # its because self.object is a TodoItem and not a TodoList. 
        # so you need to access its list property 
        # (in the model this is the foreign keys field name),
        # and we then access its id so we can go 
        # to the list that contains the todo item



class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
